Repository ini berisi kustomisasi [Firefox](https://www.mozilla.org/firefox/) yang biasa saya gunakan. Sudah diuji pada [LibreWolf](https://librewolf.net/) [v122.0-1](https://gitlab.com/librewolf-community/browser/bsys6/-/releases/122.0-1), _based on_ Firefox [v122.0](https://ftp.mozilla.org/pub/firefox/releases/122.0/)

---

Referensi atau _resource_ yang digunakan:
  - [Sidebery Addons](https://github.com/mbnuqw/sidebery)
  - [MrOtherGuy's Firefox CSS Hacks](https://github.com/MrOtherGuy/firefox-csshacks)
  - [Redundakitties's Sidebery Tweaks](https://github.com/Redundakitties/colorful-minimalist)

---

Cara penggunaan:
  - Aktifkan `toolkit.legacyUserProfileCustomizations.stylesheets` pada `about:config`
  - Buka `Root Directory` profile yang aktif, untuk lokasinya ada pada `about:profiles`
  - Masuk ke folder `chrome`, jika tidak ada bisa dibuat terlebih dahulu foldernya
  - Simpan file `userChrome.css` dan folder `chrome` yang ada di repo ini di dalam folder `chrome` yang tadi
  - Exit dan buka ulang Firefox, seharusnya jika penempatannya benar tampilannya akan berubah
